import static org.junit.Assert.*;
import org.junit.Test;

import java.io.IOException;

/** Testklass.
 * @author jaanus
 */
public class PuzzleTest {

   @Test (timeout=20000)
   public void test1() throws IOException {
      assertEquals(234, Puzzle.solve("YKS", "KAKS", "KOLM").size());
      assertEquals(1, Puzzle.solve("SEND", "MORE", "MONEY").size());
      assertEquals(2, Puzzle.solve("ABCDEFGHIJAB", "ABCDEFGHIJA", "ACEHJBDFGIAC").size());
      assertEquals("A=1, B=2, C=3, D=4, E=5, F=6, G=7, H=8, I=9, J=0", Puzzle.solve("ABCDEFGHIJAB", "ABCDEFGHIJA", "ACEHJBDFGIAC").get(0));
      assertEquals("A=2, B=3, C=5, D=1, E=8, F=4, G=6, H=7, I=9, J=0", Puzzle.solve("ABCDEFGHIJAB", "ABCDEFGHIJA", "ACEHJBDFGIAC").get(1));
   }
}

