import java.io.IOException;
import java.util.ArrayList;

public class Puzzle {

   public static void main(String[] args) throws IOException {


      String w1 = "YKS";
      String w2 = "KAKS";
      String w3 = "KOLM";

      solve(w1, w2, w3);
   }

   static ArrayList<String> solve(String w1, String w2, String w3)
   {
      usedLetter = new boolean[26];
      usedDigit = new boolean[26];
      assignedDigit = new int[26];
      solutions = new ArrayList<>();
      markLetters(w1); markLetters(w2); markLetters(w3);
      backtrack(0, w1, w2, w3);
      return solutions;
   }
   static ArrayList<String> solutions;
   static boolean[] usedLetter;
   static boolean[] usedDigit;
   static int[] assignedDigit;

   static void markLetters(String w)
   {
      for(int i = 0; i < w.length(); ++i)
         usedLetter[w.charAt(i) - 'A'] = true;
   }

   static boolean check(String w1, String w2, String w3)
   {
      if(leadingZero(w1) || leadingZero(w2) || leadingZero(w3))
         return false;
      return value(w1) + value(w2) == value(w3);
   }

   static boolean leadingZero(String w) { return assignedDigit[w.charAt(0) - 'A'] == 0; }
   static int value(String w)
   {
      int val = 0;
      for(int i = 0; i < w.length(); ++i)
         val = val * 10 + assignedDigit[w.charAt(i) - 'A'];
      return val;
   }
   static void backtrack(int char_idx, String w1, String w2, String w3)
   {
      if(char_idx == 26)
      {
         if(check(w1, w2, w3))
         {
            String solution = "";
            for(int i = 0; i < 26; ++i)
               if(usedLetter[i]) {
                  solution +=(char)(i+'A') + "=" + assignedDigit[i] + ", ";
               }
            solution = solution.substring(0, solution.length()-2);
            solutions.add(solution);
         }
         return;
      }

      if(!usedLetter[char_idx])
      {
         backtrack(char_idx + 1, w1, w2, w3);
         return;
      }
      for(int digit = 0; digit < 10; ++digit)
         if(!usedDigit[digit])
         {
            usedDigit[digit] = true;
            assignedDigit[char_idx] = digit;
            backtrack(char_idx + 1, w1, w2, w3);
            usedDigit[digit] = false;
         }
   }
}

//allikad https://gist.github.com/AhmadElsagheer/d96d1081af545834e32f3624b06c762a
